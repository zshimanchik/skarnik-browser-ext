browser.runtime.onMessage.addListener(async function (event) {
    console.log('background event received', event)

    const response = await fetch(event.url, {
        "headers": {
            "accept": "application/json, text/javascript, */*; q=0.01",
            "accept-language": "en-GB,en;q=0.7",
        },
        "body": null,
        "method": "GET",
        "mode": "cors",
        "credentials": "include",
        "redirect": "follow"
    })
    console.log(response)
    const text = await response.text()
    // console.log(text)
    const parser = new DOMParser();
    const htmlDocument = parser.parseFromString(text, "text/html");
    const el = htmlDocument.getElementById('trn')
    console.log(el)
    browser.runtime.sendMessage({ message: 'fetchResult', result: el.innerHTML })
})
